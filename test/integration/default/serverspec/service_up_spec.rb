require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "bijoe Daemons" do

  it "report is listening on port 3300" do
    expect(port(3300)).to be_listening
  end

  it "has a running service of engine" do
    expect(service("bijoe-engine")).to be_running
  end

  it "engine is listening on port 3000" do
    expect(port(3300)).to be_listening
  end

  it "has a running service of report" do
    expect(service("bijoe-node-report")).to be_running
  end

  it "identity is listening on port 4000" do
    expect(port(4000)).to be_listening
  end

  it "has a running service of identity" do
    expect(service("bijoe-identity")).to be_running
  end
end