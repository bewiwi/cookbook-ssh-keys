include_recipe 'deploy_wrapper'

data_bag(node['ssh-keys-attribute']['databag']).each do |user|
  item = data_bag_item(node['ssh-keys-attribute']['databag'], user)

  user user do
    action :create
    shell '/bin/bash'
    supports :manage_home => true
    home "/home/#{user}"
  end

  dir = File.dirname(item['ssh_private_keys_path'])
  directory dir do
    owner user
    group user
    recursive true
    action :create
  end

  file item['ssh_private_keys_path'] do
    owner user
    group user
    mode '0600'
    action :create
    content item["ssh_private_keys_content"].join("\n");
  end


end